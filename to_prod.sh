#!/bin/bash
umask 0000
cd ../../var/www
mkdir $1 && echo "created folder $1"
tar -xzvf file.tar.gz -C $1 && echo "unzipped"
rm -rf /var/www/temp2
ln -s /var/www/$1 /var/www/temp2
cd $1
cp ./.env.test ./.env
php bin/console cache:clear
php bin/console cache:warmup
